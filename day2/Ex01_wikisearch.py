import time
from selenium import webdriver


driver = webdriver.Chrome()
driver.maximize_window()

driver.get('https://www.wikipedia.org/')
driver.save_screenshot('../screenshots/wikiPage1.png')

pageTitle = driver.title
print('Title of Page 1 : ', pageTitle)

#        //*[@id="js-link-box-en"]/strong   english link
# weenglishlink   = driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/strong")

weenglishlink = driver.find_element_by_id("js-link-box-en")

weenglishlink.click()

print(' Title of page 2 : ', driver.title)
driver.save_screenshot('../screenshots/wikiPage2.png')

# //input[@id='searchInput']
# //*[@name="search']  Search Box  xpath

#    webox = driver.find_element_by_xpath("//input[@id=\'searchInput\']")
# webox = driver.find_element_by_name('search')
webox = driver.find_element_by_id('searchInput')

print(type(webox))

webox.send_keys('Selenium')

#  //input[@id='searchButton']   glass
driver.find_element_by_xpath("//input[@id='searchButton']").click()

driver.save_screenshot('../screenshots/wikiPage3.png')

assert driver.title == 'Selenium - Wikipedia'
print(' Title of page 3 : ', driver.title)



driver.back()
print(' Title of page after moving back from Page 3 : ', driver.title)

driver.forward()
print(' Title of page after moving forward : ', driver.title)












time.sleep(2)

driver.close()