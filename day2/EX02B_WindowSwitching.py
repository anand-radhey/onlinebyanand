import time
from selenium import webdriver


driver = webdriver.Firefox()
driver.maximize_window()
driver.implicitly_wait(10)

driver.get("https://www.ataevents.org/")
print('Page Title 1 : ', driver.title)


#  //*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a    Link1




link1 = driver.find_element_by_xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/figure/a")
link1.click()
print('Page Title 2 : ', driver.title)


# //*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/figure/a"
link2 = driver.find_element_by_xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/figure/a/img")
link2.click()
print('Page Title 3 : ', driver.title)


lstwh = driver.window_handles

for handle in lstwh:
    print(handle)
    driver.switch_to.window(handle)
    print(driver.title)



#driver.close()
driver.quit()