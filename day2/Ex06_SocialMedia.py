import time
from selenium import webdriver


driver = webdriver.Chrome()
driver.maximize_window()

driver.get("http://agiletestingalliance.org/")

#    //*[@id="custom_html-10"]/div/ul/li[1]/a/i     LinkedIn
#    //*[@id="custom_html-10"]/div/ul/li[2]/a/i

#    //*[@id="custom_html-10"]/div/ul/li[4]/a/i

#      //*[@id="custom_html-10"]/div/ul/li/a   for all icons (7)

lst = driver.find_elements_by_xpath("//*[@id=\"custom_html-10\"]/div/ul/li/a")   # List[WebElement]    [w1,w2,w3]

for we in lst:
    address = we.get_attribute("href")
    print(address)

# https://www.linkedin.com/company/agile-testing-alliance/
# https://twitter.com/AgileTAlliance
# https://www.youtube.com/user/AgileTestingAlliance
# https://www.instagram.com/agiletestingalliance/?hl=en
# https://www.facebook.com/AgileTestingAlliance
# https://wa.me/918433919049
# https://t.me/AgileTestingAlliance

