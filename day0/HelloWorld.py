print('Hello World')

i = 10
print(i,type(i))

for i in range(1,10):
    if i % 2 == 0:
        print(i, ' is an even number')
    else:
        print(i, ' is an odd number')

lst = [12,23,34,45,23,12,14,67]
print(lst, type(lst))

from selenium import webdriver

driver = webdriver.Chrome()

import pandas as pd