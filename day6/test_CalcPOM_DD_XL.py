import time
import pandas as pd
import openpyxl
import pytest
from selenium import webdriver
from day5.CalcPOM import ataCalcPage

def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)
    driver.get("http://ata123456789123456789.appspot.com/")

def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()

def dp_pandas():
    xlpath = "C:\\ZVA\\PythonProjects\\pythoncpsatmay20\\resources\\data\\atacalcdata.xlsx"
    df = pd.read_excel(xlpath,'calcdata')
    lol = df.values.tolist()
    return lol




def dataprovider():
    print('Inside dataprovider() method')

    xlpath = "C:\\ZVA\\PythonProjects\\pythoncpsatmay20\\resources\\data\\atacalcdata.xlsx"
    wb    = openpyxl.load_workbook(xlpath)
    sh  = wb['calcdata']
    nrows = sh.max_row
    print('No. of Rows : ', nrows)

    lol = []
    for i in range(2,nrows+1):
        lst = []
        v1 = sh.cell(i,1)
        v2 = sh.cell(i,2)
        v3 = sh.cell(i,3)
        v4 = sh.cell(i,4)
        lst.insert(0,v1.value)         # lst = ['Input 1']
        lst.insert(1, v2.value)
        lst.insert(2, v3.value)
        lst.insert(3, v4.value)
        lol.insert(i-1,lst)

    return lol


#@pytest.mark.parametrize('inputdata', dataprovider())
@pytest.mark.parametrize('inputdata', dp_pandas())
def test_calc_all(inputdata):
    print(inputdata)
    input1 =  inputdata[0]     # 12
    input2 =   inputdata[1]     # 15
    operation =   inputdata[2]     # 'add' # 'Add' 'ADD'
    expRes =   inputdata[3]     # 27
    actRes = ''
    atacalcpage = ataCalcPage(driver)

    if (operation.upper() == 'ADD'):
        actRes = atacalcpage.add(input1, input2)
    elif  (operation.upper() == 'MUL'):
        actRes = atacalcpage.multiply(input1, input2)
    elif  (operation.upper() == 'COMP'):
        actRes = atacalcpage.compare(input1, input2)
    elif  (operation.upper() == 'EUC+'):
        actRes = atacalcpage.eucplus(input1, input2)
    else:
        print('Invalid Data Point')



    print(expRes, actRes)
    assert expRes == actRes

#
#
#
# def test_calc_add1():
#
#     n1 = 12
#     n2 = 15
#     expRes = 27
#     print(n1,'   ', n2, '   ', expRes)
#
#     atacalcpage = ataCalcPage(driver)
#     actRes = atacalcpage.add(n1,n2)
#     print(actRes)
#     assert expRes == actRes
#
# def test_calc_add2():
#
#     n1 = 0
#     n2 = 500
#     expRes = 500
#     print(n1,'   ', n2, '   ', expRes)
#
#     atacalcpage = ataCalcPage(driver)
#     actRes = atacalcpage.add(n1,n2)
#     print(actRes)
#     assert expRes == actRes
#
#
# def test_calc_mul():
#     n1 = 10
#     n2 = 15
#     expRes = 150
#     print(n1, '   ', n2, '   ', expRes)
#
#     atacalcpage = ataCalcPage(driver)
#     actRes = atacalcpage.multiply(n1, n2)
#     print(actRes)
#     assert expRes == actRes
#
#
#



