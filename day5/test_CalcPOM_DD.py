import time
import pytest
from selenium import webdriver
from day5.CalcPOM import ataCalcPage

def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)
    driver.get("http://ata123456789123456789.appspot.com/")

def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()

def dataprovider():
    print('Inside dataprovider() method')
    lol =[  [12,	13,	'add',	25],
            [10,	20,	'Mul',	200],
            [18,	32,	'Add',	50],
            [500,	100,'comp',	500],
            [50,	12,	'add',	62]             ]     # [  [12,13,'Add',27] , [12,13,'Add',27] ,[12,13,'Add',27]  ]
    return lol


@pytest.mark.parametrize('inputdata', dataprovider())
def test_calc_all(inputdata):
    print(inputdata)
    input1 =  inputdata[0]     # 12
    input2 =   inputdata[1]     # 15
    operation =   inputdata[2]     # 'add' # 'Add' 'ADD'
    expRes =   inputdata[3]     # 27
    actRes = ''
    atacalcpage = ataCalcPage(driver)

    if (operation.upper() == 'ADD'):
        actRes = atacalcpage.add(input1, input2)
    elif  (operation.upper() == 'MUL'):
        actRes = atacalcpage.multiply(input1, input2)
    elif  (operation.upper() == 'COMP'):
        actRes = atacalcpage.compare(input1, input2)
    else:
        print('Invalid Data Point')



    print(expRes, actRes)
    assert expRes == actRes

#
#
#
# def test_calc_add1():
#
#     n1 = 12
#     n2 = 15
#     expRes = 27
#     print(n1,'   ', n2, '   ', expRes)
#
#     atacalcpage = ataCalcPage(driver)
#     actRes = atacalcpage.add(n1,n2)
#     print(actRes)
#     assert expRes == actRes
#
# def test_calc_add2():
#
#     n1 = 0
#     n2 = 500
#     expRes = 500
#     print(n1,'   ', n2, '   ', expRes)
#
#     atacalcpage = ataCalcPage(driver)
#     actRes = atacalcpage.add(n1,n2)
#     print(actRes)
#     assert expRes == actRes
#
#
# def test_calc_mul():
#     n1 = 10
#     n2 = 15
#     expRes = 150
#     print(n1, '   ', n2, '   ', expRes)
#
#     atacalcpage = ataCalcPage(driver)
#     actRes = atacalcpage.multiply(n1, n2)
#     print(actRes)
#     assert expRes == actRes
#
#
#



