
class ataCalcPage:
    
    def __init__(self,driverobj):
        self.driver = driverobj

        # //*[@id="ID_nameField1"]    input1
        # //*[@id="ID_nameField2"]   input2
        # //*[@id="gwt-uid-2"]      mul  radiobutton
        # //*[@id="gwt-uid-1"]      add radiobutton
        # //*[@id="ID_calculator"]   Calculate Button
        # //*[@id="ID_nameField3"]   Result box
        # //*[@id="gwt-uid-4"]      comp radiobutton

        self.input1    = self.driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
        self.input2 =  self.driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")

        self.addradio = self.driver.find_element_by_xpath("//*[@id='gwt-uid-1']")
        # //*[@id="gwt-uid-2"]      mul  radiobutton
        self.mulradio = self.driver.find_element_by_xpath("//*[@id='gwt-uid-2']")
        self.compradio = self.driver.find_element_by_xpath("//*[@id='gwt-uid-4']")

        self.calc = self.driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
        self.resultBox = self.driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")


    def add(self,num1,num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.addradio.click()

        self.calc.click()

        actRes = int(self.resultBox.get_attribute('value'))

        return actRes

    def multiply(self,num1,num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.mulradio.click()

        self.calc.click()

        actRes = int(self.resultBox.get_attribute('value'))

        return actRes

    def compare(self, num1, num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.compradio.click()

        self.calc.click()

        actRes = int(self.resultBox.get_attribute('value'))

        return actRes



