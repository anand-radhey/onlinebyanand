import time
from selenium import webdriver


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()

    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()

def test_calc_add():
    driver.get("http://ata123456789123456789.appspot.com/")
    num1 = 12
    num2 = 30
    expRes = 42

    # //*[@id="ID_nameField1"]    input1
    input1 = driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
    input1.clear()
    input1.send_keys(str(num1))

    # //*[@id="ID_nameField2"]   input2
    input2 = driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")
    input2.clear()
    input2.send_keys(str(num2))

    # //*[@id="gwt-uid-2"]      mul  radiobutton
    # //*[@id="gwt-uid-1"]      add radiobutton
    addradio = driver.find_element_by_xpath("//*[@id='gwt-uid-1']")
    addradio.click()

    # //*[@id="ID_calculator"]   Calculate Button
    calc = driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
    calc.click()


    # //*[@id="ID_nameField3"]   Result box
    resultBox = driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")
    actRes = int(resultBox.get_attribute('value'))

    print('actRes : ', actRes, ' And ExpectedRes :', expRes)

    assert expRes == actRes



def test_calc_multiply():
    driver.get("http://ata123456789123456789.appspot.com/")
    num1 = 10
    num2 = 20
    expRes = 200

    # //*[@id="ID_nameField1"]    input1
    input1 = driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
    input1.clear()
    input1.send_keys(str(num1))

    # //*[@id="ID_nameField2"]   input2
    input2 = driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")
    input2.clear()
    input2.send_keys(str(num2))

    # //*[@id="gwt-uid-2"]      mul  radiobutton
    mulradio = driver.find_element_by_xpath("//*[@id='gwt-uid-2']")
    mulradio.click()

    # //*[@id="ID_calculator"]   Calculate Button
    calc = driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
    calc.click()


    # //*[@id="ID_nameField3"]   Result box
    resultBox = driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")
    actRes = int(resultBox.get_attribute('value'))

    print('actRes : ', actRes, ' And ExpectedRes :', expRes)

    assert expRes == actRes












