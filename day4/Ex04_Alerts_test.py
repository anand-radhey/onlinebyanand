from selenium import webdriver



def setup():
    global driver
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    driver.quit()

def test_validate_JSAlert():
    print('Inside test_validate_JSAlert() method ')
    driver.get("https://the-internet.herokuapp.com/javascript_alerts")

    #   //*[@id="content"]/div/ul/li[1]/button
    first = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[1]/button")
    first.click()

    alert1   = driver.switch_to.alert

    alert1.accept()
    expResult = "You successfuly clicked an alert"

    #   //*[@id="result"]
    actualRes = driver.find_element_by_xpath("//*[@id=\"result\"]").text
    print(expResult)
    print(actualRes)
    if (expResult == actualRes ):
        print('You successfully clicked an alert')

def test_validate_JSAlert2():
    print('Inside test_validate_JSAlert2()')
    driver.get('https://the-internet.herokuapp.com/javascript_alerts')
    # // *[ @ id = "content"] / div / ul / li[1] / button
    first = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[2]/button")
    first.click()

    alert1 = driver.switch_to.alert

    alert1.dismiss()
    expResult = 'You clicked: Cancel'
    # // *[ @ id = "result"]

    actualres = driver.find_element_by_xpath("//*[@id='result']").text

    assert expResult == actualres


    #
    # if(expResult == actualres):
    #     print('You clicked: Cancel')

    first1 = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[2]/button")
    first1.click()

    alert = driver.switch_to.alert

    alert.accept()
    expResult1 = 'You clicked: Ok'
    # // *[ @ id = "result"]

    actualres1 = driver.find_element_by_xpath("//*[@id='result']")

    if (expResult1 == actualres1):
        print('You clicked: Ok')


