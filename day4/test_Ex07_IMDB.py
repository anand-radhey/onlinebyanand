import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import time


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()

    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()

def dataGenerator():
    lst = [['Raazi','Meghna Gulzar','Vicky'], ['Baazigar', 'Mastan', 'Shah Rukh Khan']]
    return lst

@pytest.mark.parametrize('data',dataGenerator())
def test_search(data):
    movie =  data[0]        # 'Raazi'
    expDir = data[1]   # 'Meghna Gulzar'
    expStar = data[2]   # 'Vicky'
    print(data)

    driver.get('https://www.imdb.com/')

    search = driver.find_element_by_xpath("//*[@id='suggestion-search']")
    search.send_keys(movie)
    search.send_keys(Keys.ENTER)
    #
    # icon = driver.find_element_by_xpath("//*[@id='suggestion-search-button']")
    # icon.click()

    movieLink = driver.find_element_by_xpath("//*[@id='main']/div/div[2]/table/tbody/tr[1]/td[2]/a")
    movieLink.click()


    #   //*[@id="title-overview-widget"]/div[2]/div[1]/div[4]/h4     Stars

    #  //*[ @class='inline'  and contains( text(),'Star'  )]/following-sibling::*

    lstStars = driver.find_elements_by_xpath("//*[ @class='inline'  and contains( text(),'Star'  )]/following-sibling::*")

    starFound = False
    for star in lstStars:
        starname = star.text
        print(starname)
        if ( expStar  in starname ):
            starFound = True
            print('Found The Star ', expStar , '  :::::   ', starname)
            break





