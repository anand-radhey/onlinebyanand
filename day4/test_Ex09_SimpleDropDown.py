from selenium import webdriver
from selenium.webdriver.support.select import Select
import time


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()

    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('Inside teardown() method')
    time.sleep(3)
    driver.quit()

def test_verify_dropdown():
    print('Inside test_verify_dropdown() method')
    driver.get("file:///C:/ZVA/PythonProjects/pythoncpsatmay20/resources/data/dropdown.html")
    # /html/body/form/select    dropdown

    dd = driver.find_element_by_xpath("/html/body/form/select")
    selectObject = Select(dd)

    lstOptions   = selectObject.options

    for opt in lstOptions:
        print(opt.text)

    time.sleep(3)
    selectObject.select_by_visible_text("CP-AAT")

    time.sleep(3)
    selectObject.select_by_value("3")

    time.sleep(3)
    selectObject.select_by_index(3)

    # driver.switch_to.frame()
    
    






