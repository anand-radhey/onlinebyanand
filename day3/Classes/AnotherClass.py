from day3.Classes.PersonClass import Person


class Student(Person):   # Student(child) is a subclass of Person(Parent)

    def __init__(self, name, age, std):
        super().__init__(name,age)
        self.std = std

    def greetings(self):
        print('Greetings from : ', self.name)

    def sayHello(xyz):
        print('I am student of standard ',xyz.std, ' And Saying Hello To All')




st1 = Student('Rajesh',13, 'VIII')
print(st1.name, st1.age, st1.std)
st1.sayHello()
st1.greetings()


p1 = Person('Abhishek', 29)
p1.sayHello()


