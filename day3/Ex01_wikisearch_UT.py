import time
import unittest
from selenium import webdriver



class MyTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('entered setUpClass() the Class Method ')
        global driver
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.implicitly_wait(10)


    def setUp(self):
        print('entered setUp() ')


    def tearDown(self):
        print('entered tearDown() ')

    @classmethod
    def tearDownClass(cls):
        print('entered tearDownClass() at Class level ')
        time.sleep(2)
        driver.quit()




    def test1Wiki(self):
        print('Entered test1Wiki() ')

        driver.get('https://www.wikipedia.org/')
        driver.save_screenshot('../screenshots/wikiPage1.png')
        pageTitle = driver.title
        print(pageTitle)

        # "//*[@id=\"js-link-box-en\"]/strong"

        # eng = driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/strong")
        # eng.click()
        eng = driver.find_element_by_id("js-link-box-en")
        eng.click()

        print('Title of Page 2', driver.title)
        driver.save_screenshot('../screenshots/wikiPage2.png')
        search = driver.find_element_by_xpath("//*[@id=\"searchInput\"]")
        search.send_keys('Selenium')
        search = driver.find_element_by_name("search")
        searchbar = driver.find_element_by_xpath("//*[@id=\"searchButton\"]").click()
        print('Title of Page 3', driver.title)
        # driver.save_screenshot('../screenshots/wikiPage3.png')

        self.assertEqual('Selenium - Wikipedia', driver.title)
       # assert driver.title == 'Selenium - Wikipedia'
        driver.back()
        print('Title of page 3:', driver.title)
        driver.forward()
        print('Title of page currentpage:', driver.title)



    def test2Wiki(self):
        print('Entered test2Wiki() ')

        driver.get('https://www.wikipedia.org/')
        driver.save_screenshot('../screenshots/wikiPage1.png')
        pageTitle = driver.title
        print(pageTitle)

        # "//*[@id=\"js-link-box-en\"]/strong"

        # eng = driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/strong")
        # eng.click()
        eng = driver.find_element_by_id("js-link-box-en")
        eng.click()

        print('Title of Page 2', driver.title)
        driver.save_screenshot('../screenshots/wikiPage2.png')
        search = driver.find_element_by_xpath("//*[@id=\"searchInput\"]")
        search.send_keys('CPSAT')
        search = driver.find_element_by_name("search")
        searchbar = driver.find_element_by_xpath("//*[@id=\"searchButton\"]").click()
        print('Title of Page 3', driver.title)
        # driver.save_screenshot('../screenshots/wikiPage3.png')

        self.assertEqual('Selenium - Wikipedia', driver.title)
       # assert driver.title == 'Selenium - Wikipedia'
        driver.back()
        print('Title of page 3:', driver.title)
        driver.forward()
        print('Title of page currentpage:', driver.title)
